# ANZ Loan Estimation Calculator Automation

This WebUI Automation framework can be used to write test scripts to automate a web application using simple generic actions on elements and the locators passed directly from the Cucumber scenarios.


# Framework details:
* This test automation framework is based on BDD model using Cucumber,Selenium and Java
* Like any standard BDD framework, the test can be run using command line which makes it available for CI/CD integratrion. Tests can also be run using the Runner class

# Framework advantages:
* Stepdefinitions are written in a way that they can be reused by just passing the required locator and values from the steps of a scenario.
* All the actions are handled in Feature file itself to make it easier for the user to understand what actions are being performed.
* The framework uses "SimpleBy" so that the framework will match the locator thats being passed with the type of the locator, i.e; there is no need to specifiy if the locator is an xpath or id or any other locator type.
* The framework generated three reports which are the Cucumber report,Extent report and the Cucumber scenario report.
* The framework has effective wait management.


### Setting up the framework to run in your local machine:

Setting up this framework to run in your local machine is easy as it can directly be cloned from the GIT repo.
> **Please note that GIT,Maven and chrome driver needs to be installed in the local machine inorder to run the tests.**

Once the repo is cloned into your local machine, open the command prompt and navigate to the project directory and run the following command:

```sh
mvn test -Dcucumber.options="--tags @LoanCalculator"
```

To run the tests in headless mode, following command can be used:
```sh
mvn test -Dcucumber.options="--tags @LoanCalculator" -Dheadless="true"
```

Each run produce new results and can be found under the following paths:

> Extent report html:-**./WebReport.html**

> Cucumber report html:- **./target/cucumber-reports/report.html**

> Cucumber feature level report:- **./target/cucumber-reports/cucumber-html-reports/overview-features.html**

> JSON Report:-**./target/cucumber-reports/report.json**



