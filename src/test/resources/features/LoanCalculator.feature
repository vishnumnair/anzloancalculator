Feature: Loan Calculator UI Testing

  @LoanCalculator
  Scenario: Test to validate Loan Estimation calculator
    Given I launch "CHROME" browser
    And I navigate to the url "https://www.anz.com.au/personal/home-loans/calculators-tools/much-borrow/"
    And I click on the element "Single"
    And I select value "0" of element "//label[text()='Number of dependants']/following::select[1]"
    And I click on the element "Home to live in"
    And I enter "80000" in element "//label[contains(text(),'Your income')]/following::input[1]"
    And I enter "10000" in element "//label[contains(text(),'other income')]/following::input[1]"
    And I enter "500" in element "expenses"
    And I enter "100" in element "otherloans"
    And I enter "10000" in element "credit"
    And I click on the element "btnBorrowCalculater"
    And I wait for element "//span[@aria-live='assertive' and @class='borrow__result__text']"
    Then I compare text in element "borrowResultTextAmount" to "$500,000"
    And I take Screenshot
    And I click on the element "Start over"
    Then I compare text in element "expenses" to ""
    And I compare text in element "//label[@id='q1q3']/following::li[@class='selected']" to "Home to live in"




