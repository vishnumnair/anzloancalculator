package com.automation.stepDefinitions;

import com.automation.Utils.AutomationException;
import com.automation.Utils.Reporter;
import com.automation.web.BrowserNames;
import com.automation.Utils.Properties;
import com.automation.web.WebAutomation;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.After;

import static org.junit.Assert.assertEquals;

public class StepDefinitions {
   private WebAutomation web;
   private Scenario scenario;

    @Before
    public void init(Scenario scenario){
        this.scenario=scenario;
        Reporter.getInstance().createTest(scenario.getName(), scenario.getName());
    }

    @Given("^I launch \"([^\"]*)\" browser$")
    public void launchBrowser(String browserName) throws AutomationException {
        web = new WebAutomation(BrowserNames.valueOf(browserName),Boolean.valueOf(Properties.read("headless")));
        scenario.log("Launched the browser successfully.");
    }

    @And("^I navigate to the url \"([^\"]*)\"$")
    public void launchApplication(String url) throws AutomationException {
        web.getURL(url);
        scenario.log("Navigated to the url "+url+" successfully.");
    }

    @When("^I click on the element \"([^\"]*)\"$")
    public void click_Element(String locator) throws AutomationException {
        web.click(locator);
        scenario.log("Clicked the element successfully.");
    }
    @When("^I enter \"([^\"]*)\" in element \"([^\"]*)\"$")
    public void enter_Text(String text,String locator) throws AutomationException {
        web.setText(locator,text);
        scenario.log("Entered the text successfully.");
    }
    @When("^I compare text in element \"([^\"]*)\" to \"([^\"]*)\"$")
    public void compare_Text(String locator, String expectedValue) throws AutomationException {
        assertEquals("ERROR: The values from response is not matching; Actual: " , expectedValue.trim(), web.getText(locator) );
        scenario.log("Compared the text with actual and expected value successfully.");
    }

    @When("^I take Screenshot$")
    public void screenshot() throws AutomationException {
        scenario.attach(web.screenshot(),"image/png","Application");
        scenario.log("Captured the screenshot successfully.");
    }
    @When("^I select value \"([^\"]*)\" of element \"([^\"]*)\"$")
    public void select_Value(String value, String locator) throws AutomationException {
        web.select(locator,value);
        scenario.log("Selected the element successfully.");
    }
    @When("^I wait for element \"([^\"]*)\"$")
    public void wait_For_Element( String locator) throws AutomationException {
        web.wait(locator);
        scenario.log("Element exists successfully.");
    }

    @After
    public void cleanUp(){
        web.destroy();
    }
}
