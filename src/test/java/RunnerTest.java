import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(plugin={"pretty","html:target/cucumber-reports/report.html","junit:target/cucumber-reports/report.xml","json:target/cucumber-reports/report.json"},
        glue = {"com.automation.stepDefinitions"},
        tags = "@LoanCalculator"
)
public class RunnerTest {

}
