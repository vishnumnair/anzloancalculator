package com.automation.web;

import com.automation.Utils.AutomationException;
import com.automation.Utils.Reporter;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebDriver;

/**
 * Factory Class for Instantiating new WebDriver instance based on
 * the Browser Name given
 * It will run the WebDriver in normal or headless mode
 */
public class DriverFactory {


    /**
     * To get the new WebDriver instance for the browser mentioned
     * This is used when browser is to be run as Headless mode
     *
     * @param browserName Name of the browser for which WebDriver Instance is needed
     * @param headless    if 'true' browser is run in headless mode, else if 'false' it would run normally
     * @return new WebDriver Instance
     * @throws AutomationException exception handling
     */


    public  WebDriver getDriver( BrowserNames browserName, Boolean headless) throws AutomationException {
        Reporter.getInstance().getLogger().log(Status.PASS, "Browser in use is  : [" + browserName + "]");
        switch (browserName) {
            case FIREFOX:
            case IE:
                throw new AutomationException("Not implemented");
            case CHROME:
                ChromeDriverImpl cDI=new ChromeDriverImpl( headless);
                return cDI.getWebDriver();
            default:
                throw new AutomationException("Could Not Instantiate Driver as Browser Requested Not found");
        }
    }




}
