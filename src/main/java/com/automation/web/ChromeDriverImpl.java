package com.automation.web;

import com.automation.Utils.AutomationException;
import com.automation.Utils.Properties;
import com.automation.Utils.Reporter;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Chrome Driver instantiation class build in Factory Pattern
 * It helps in getting the WebDriver instance for Chrome Browser Driver
 */
public class ChromeDriverImpl  {

    private WebDriver driver;

    /**
     * Constructor to create a new WebDriver instance for Chrome Browser driver
     *
     * @param headless if true= runs chrome in headless mode, else normally if false
     * @throws AutomationException for exception handling
     */



    public ChromeDriverImpl( boolean headless) throws AutomationException {
        try {
            int TIMEOUT= (StringUtils.isNumeric(Properties.read("timeout")))?Integer.parseInt(Properties.read("timeout")):60;

            ChromeOptions options = new ChromeOptions();
            Map<String, Object> prefs = new HashMap<>();
            prefs.put("profile.default_content_settings.popups", 0);
            prefs.put("safebrowsing.enabled", true);
            options.setExperimentalOption("prefs", prefs);
            if (headless) options.addArguments("--headless");

            options.addArguments("--ignore-certificate-errors");
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--disable-gpu");
            options.addArguments("disable-infobars");
            options.addArguments("--start-maximized");
            ChromeDriverService driverService = ChromeDriverService.createDefaultService();
            LoggingPreferences logPrefs = new LoggingPreferences();
            logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
            driver = new ChromeDriver(driverService, options);
            driver.manage().timeouts().setScriptTimeout(TIMEOUT, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(TIMEOUT, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
            driver.manage().window().maximize();



            Reporter.getInstance().flushReport();
        } catch (Exception e) {
            e.printStackTrace();

            throw new AutomationException("The chrome driver not found", e);
        }
    }


    public WebDriver getWebDriver() {
        return driver;
    }

}
