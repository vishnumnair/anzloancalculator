package com.automation.web;

/**
 * Valid names for Browsers
 * For which WebDriver instances could be instantiated
 */
public enum BrowserNames {
    CHROME,
    FIREFOX,
    IE
}
