package com.automation.web;

import com.automation.Utils.AutomationException;
import com.automation.Utils.Reporter;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Base64;


public class WebAutomation {
    private static final SimpleBy SIMPLE_BY = new SimpleByImpl();
    protected static final int TIMEOUT = 30;
    private WebDriver driver;

    public WebAutomation(BrowserNames browser, Boolean headless) throws AutomationException {
        DriverFactory dI = new DriverFactory();
        this.driver = dI.getDriver(browser, headless);
    }

    public WebAutomation(WebDriver driver) {
        this.driver = driver;
    }
    public WebDriver getDriver() {
        return driver;
    }


    public void wait(String locator) throws AutomationException {
        try{

            SIMPLE_BY.getElement(
                    driver,
                    locator,
                    TIMEOUT,
                    ExpectedConditions::elementToBeClickable);


           Reporter.getInstance().getLogger().log(Status.PASS, "Completed waiting for element [<b><font  color='green'>" +  locator+ "</font></b>]");

        }catch (Exception e){
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step click on [<b><font  color='red'>" + locator + "</font></b>]" + e.getMessage());
            screenshot();

            throw new AutomationException("Unable to process element [" +locator + "]", e);
        }finally {
            Reporter.getInstance().flushReport();
        }
    }


    /**
 * gotoURl Functions implemented with simple BY
 */

public void getURL(String url) throws AutomationException {
    try{
        driver.get(url);
        new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
        {
            assert wd != null;
            return ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete");
        });

        Reporter.getInstance().getLogger().log(Status.PASS, "Completed opening URL[<b><font  color='green'>" +  url+ "</font></b>]");

    }catch (Exception e){
         Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step click on [<b><font  color='red'>" + url + "</font></b>]" + e.getMessage());
        screenshot();

        throw new AutomationException("Unable to process element [" +url + "]", e);
    }finally {
        Reporter.getInstance().flushReport();
    }
}

    /**
     * Click Functions implemented with simple BY
     */
    public  void click(String locator) throws AutomationException {

        try {

            SIMPLE_BY.getElement(
                    driver,
                    locator,
                    TIMEOUT,
                    ExpectedConditions::elementToBeClickable)
                    .click();

            Reporter.getInstance().getLogger().log(Status.PASS, "Completed running step click on [<b><font  color='green'>" +  locator+ "</font></b>]");

        }catch (Exception e){
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step click on [<b><font  color='red'>" + locator + "</font></b>]" + e.getMessage());
            screenshot();

            throw new AutomationException("Unable to process element [" +locator + "]", e);
        }
        finally {
            Reporter.getInstance().flushReport();
        }
    }


    /**
     * Get text implemented by Simple BY
     * @return
     */


    public String getText(String locator) throws AutomationException {

        try {

           String text=SIMPLE_BY.getElement(
                    driver,
                    locator,
                    TIMEOUT,
                    ExpectedConditions::visibilityOfElementLocated)
                    .getText();

            Reporter.getInstance().getLogger().log(Status.PASS, "Completed running step get text on [<b><font  color='green'>" +  locator+ "</font></b>] : value retrieved "+text);

            return text;
        }catch (Exception e){
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step get text on [<b><font  color='red'>" + locator + "</font></b>]" + e.getMessage());
            screenshot();

            throw new AutomationException("Unable to process element [" +locator + "]", e);
        }finally {
            Reporter.getInstance().flushReport();
        }

    }




    /**
     * set text implemented by Simple BY
     */
    public  void setText(String locator,String value) throws AutomationException {

        try {

           SIMPLE_BY.getElement(
                    driver,
                    locator,
                    TIMEOUT,
                    ExpectedConditions::elementToBeClickable)
                    .sendKeys(value);

            Reporter.getInstance().getLogger().log(Status.PASS, "Completed running step set text on [<b><font  color='green'>" +  locator+ "</font></b>] : value entered "+value);

        }catch (Exception e){
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step get text on [<b><font  color='red'>" + locator + "</font></b>]" + e.getMessage());
            screenshot();

            throw new AutomationException("Unable to process element [" +locator + "]", e);
        }
        finally {
            Reporter.getInstance().flushReport();
        }
    }
    public  void select(String locator,String value) throws AutomationException {

        try {

            Select select=new Select(SIMPLE_BY.getElement(
                    driver,
                    locator,
                    TIMEOUT,
                    ExpectedConditions::elementToBeClickable));
            select.selectByVisibleText(value);

            Reporter.getInstance().getLogger().log(Status.PASS, "Completed running step set text on [<b><font  color='green'>" +  locator+ "</font></b>] : value entered "+value);

        }catch (Exception e){
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step get text on [<b><font  color='red'>" + locator + "</font></b>]" + e.getMessage());
            screenshot();

            throw new AutomationException("Unable to process element [" +locator + "]", e);
        }
        finally {
            Reporter.getInstance().flushReport();
        }
    }



    /**
     *clear implemented by Simple BY
     */
    public  void clear(String locator) throws AutomationException {

        try {
            //   String loc= locator.toString().replaceAll("By.(.*?):","");
            SIMPLE_BY.getElement(
                    driver,
                    locator,
                    TIMEOUT,
                    ExpectedConditions::elementToBeClickable)
                    .clear();

            Reporter.getInstance().getLogger().log(Status.PASS, "Completed running step clear on [<b><font  color='green'>" +  locator+ "</font></b>] ");

        }catch (Exception e){
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step clear on [<b><font  color='red'>" + locator + "</font></b>]" + e.getMessage());
            screenshot();

            throw new AutomationException("Unable to process element [" +locator + "]", e);
        }
        finally {
            Reporter.getInstance().flushReport();
        }
    }



    /**
     *checkbox implemented by Simple BY
     */
    public  void checkBox(String locator,boolean check) throws AutomationException {

        try {
            WebElement element = SIMPLE_BY.getElement(
                    driver,
                    locator,
                    TIMEOUT,
                    ExpectedConditions::elementToBeClickable);

            if ((check && !element.isSelected()) || (!check && element.isSelected())){
                element.click();
            }

            Reporter.getInstance().getLogger().log(Status.PASS, "Completed running step checkbox on [<b><font  color='green'>" +  locator+ "</font></b>] ");

        }catch (Exception e){
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step on element [<b><font  color='red'>" + locator + "</font></b>]" + e.getMessage());
            screenshot();

            throw new AutomationException("Unable to process element [" +locator + "]", e);
        }
        finally {
            Reporter.getInstance().flushReport();
        }
    }

    /**
     *switchtoIframe implemented by Simple BY
     */
    public  void switchToIframe(String locator) throws AutomationException {

        try {

            WebElement element = SIMPLE_BY.getElement(
                    driver,
                    locator,
                    TIMEOUT,
                    ExpectedConditions::elementToBeClickable,ExpectedConditions::visibilityOfElementLocated);

            driver.switchTo().frame(element);

            Reporter.getInstance().getLogger().log(Status.PASS, "Completed running step switch Iframe on [<b><font  color='green'>" +  locator+ "</font></b>] ");

        }catch (Exception e){
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process switch Iframe [<b><font  color='red'>" + locator + "</font></b>]" + e.getMessage());
            screenshot();

            throw new AutomationException("Unable to process element [" +locator + "]", e);
        }
        finally {
            Reporter.getInstance().flushReport();
        }
    }

    public void wait(String locator,ExpectedConditionCallback ... expectedConditionCallback) throws  AutomationException {
        try{
            SIMPLE_BY.getElement(
                    driver,
                    locator,
                    TIMEOUT,
                    expectedConditionCallback);
            Reporter.getInstance().getLogger().log(Status.PASS, "Completed waiting for element [<b><font  color='green'>" +  locator+ "</font></b>]");
        }catch (Exception e){
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step click on [<b><font  color='red'>" + locator + "</font></b>]" + e.getMessage());
            screenshot();
            throw new AutomationException("Unable to process element [" +locator + "]", e);
        }finally {
            Reporter.getInstance().flushReport();
        }
    }

    public byte[] screenshot() throws AutomationException {
        try {
            byte[] screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            String screenv = "data:image/jpg;base64," + Base64.getEncoder().encodeToString(screen);

            Reporter.getInstance().getLogger().log(Status.INFO, "<ul class='screenshots'><li><img  width='80%' src='" + screenv + "' data-src='" + screenv + "'></li></ul>");
            Reporter.getInstance().flushReport();
            return screen;
        } catch (Exception e) {
            Reporter.getInstance().getLogger().log(Status.FAIL, "Unable to process step [<b><font  color='red'>screenshot</font></b>] " + e.getMessage());
            Reporter.getInstance().flushReport();
            throw new AutomationException("Unable to process take screenshot", e);
        }
    }


    public void refresh(){
        driver.navigate().refresh();
    }

   

    public void navigateToUrl(String url){
        driver.get(url);

    }

    public  void  destroy(){
        if(driver!=null){
            driver.close();
            driver.quit();
        }
    }


}
