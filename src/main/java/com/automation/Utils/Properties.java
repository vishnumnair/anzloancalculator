package com.automation.Utils;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Set;

public class Properties {

    private static Properties properties;
    private static java.util.Properties prop = new java.util.Properties();
    private static ExtentTest logger = Reporter.getInstance().getLogger();
    private boolean log = true;

    private Properties() {
    }




    /**
     * To show the Properties Details in Reporting
     * if 'true' show the properties else keep it 'false'
     *
     * @param log true or false value
     * @return current Properties instance
     */
    public static Properties log(boolean log) {
        instance().log = log;
        return instance();
    }

    /**
     * To Return value from a Particular Key from Properties
     *
     * @param key name of the Key for which value need to be returned
     * @return String value for the Property Key
     * @throws AutomationException for exception handling
     */
    public static String read(String key) throws AutomationException {
        String returnValue = null;
        if(System.getProperty(key)!=null&&!System.getProperty(key).equalsIgnoreCase("")){
            returnValue= System.getProperty(key);
        }else {

            Iterator<File> it = FileUtils.iterateFiles(new File(System.getProperty("user.dir")), new String[]{"properties"}, true);

            try {
                while (it.hasNext()) {
                    prop.load(new FileInputStream( it.next().getPath()));
                    Set<Object> keys = prop.keySet();
                    for (Object k : keys) {
                       if (k.toString().equalsIgnoreCase(key)){
                            returnValue = prop.getProperty(key);
                            break;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.log(Status.FAIL, "Error reading properties file " + it.next().getPath());
                Reporter.getInstance().flushReport();
                throw new AutomationException("Error reading properties file " + it.next().getPath(), e);
            }
        }

            if (instance().log) {
                if (!key.contains("password"))
                    logger.log(Status.INFO, "<pre> Properties Value Retrieved : |" + key + " : " + returnValue + "| </pre>");
                Reporter.getInstance().flushReport();
            }
            return returnValue;

    }


    private static Properties instance() {
        if (properties == null) {
            properties = new Properties();
        }
        return properties;

    }
}
