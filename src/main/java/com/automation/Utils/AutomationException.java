package com.automation.Utils;


public class AutomationException extends Exception {


    public AutomationException(final String message) {
        super(message);
    }


    public AutomationException(final Exception message) {
        super(message);
    }


    public AutomationException(final String message, final Exception exception) {
        super(message, exception);
    }

}


